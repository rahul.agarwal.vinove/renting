import express from 'express';
import { itemRoutes } from '../controllers/itemController';

const router = express.Router();

router.use('/', itemRoutes);

export default router;