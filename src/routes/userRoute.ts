import express from 'express';
import { userRoutes } from '../controllers/userController';

const router = express.Router();

router.post('/', userRoutes);

export default router;