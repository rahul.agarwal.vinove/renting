import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
// import { MongoHelper } from './../mongo.helper';
import mongoose from 'mongoose';
import userRoutes from './routes/userRoute';
import itemRoutes from './routes/itemRoute';

const MONGO_URI = 'mongodb://127.0.0.1:27017/rent';

const app = express();

app.use(bodyParser.json({limit: '50mb'}));

app.use('/user', userRoutes);
app.use('/item', itemRoutes);

app.set('port', 3100);

http.createServer(app).listen(app.get('port'), async () => {
    console.log('Express server listening on port ' + app.get('port'));

    mongoose.connect(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });

    mongoose.connection.on('open', () => {
        console.log("Mongo DB connected successfully.");
    });

    mongoose.connection.on('error', () => {
        console.log("Mongo DB connection error.");
    });
});