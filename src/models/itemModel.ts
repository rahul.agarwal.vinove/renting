import { model, Schema } from "mongoose";

const itemSchema = new Schema(
    {
      name: {
        type: String,
        required: true,
      },
      rentPrice: {
        type: Number,
        required: true,
      },
      manufactureDate: {
        type: Date,
        required: true,
      },
      actualCost: {
          type: Number,
          required: true
      },
      userId: {
          type: String,
          required: true
      }
    },
    { timestamps: true }
  )
  
  const ItemModel = model("Items", itemSchema);

  export {
      ItemModel
  }