import { model, Schema } from "mongoose";

const userSchema = new Schema(
    {
      firstName: {
        type: String,
        required: true,
      },
      lastName: {
        type: String,
        required: true,
      },
      address: {
        type: String,
        required: true,
      },
    },
    { timestamps: true }
  )
  
  const UserModel = model("User", userSchema);

  export {
      UserModel
  }