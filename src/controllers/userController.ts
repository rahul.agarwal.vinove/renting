import express, { Response, Request } from 'express';
import { UserModel } from '../models/userModel';

const userRoutes = express.Router();

userRoutes.post('/', async (req: Request, res:Response) => {
    const firstName = req.body['firstName'];
    const lastName = req.body['lastName'];
    const address = req.body['address'];

    const user = new UserModel({ firstName, lastName, address });

    await user.save().then(user => {
        res.send(`User has been created successfully.`);
    }).catch(err => {
        res.send(err);
    });
});

export {
    userRoutes
}