import express, { Response, Request } from 'express';
import { ItemModel } from '../models/itemModel';

const itemRoutes = express.Router();

itemRoutes.post('/', async (req: Request, res:Response) => {
    const name = req.body['name'];
    const rentPrice = req.body['rentPrice'];
    const manufactureDate = req.body['manufactureDate'];
    const actualCost = req.body['actualCost'];
    const userId = req.body['userId'];

    const item = new ItemModel({ name, rentPrice, manufactureDate, actualCost, userId });

    await item.save().then(user => {
        res.send(`Item has been added successfully.`);
    }).catch(err => {
        res.send(err);
    });
});

itemRoutes.put('/:itemId', async (req: Request, res: Response) => {
    const itemId = req.params['itemId'];
    const item = new ItemModel(req.body);
    
    await ItemModel.findByIdAndUpdate(itemId, item).then(() => {
        res.send("Item updated successfully.");
    }).catch(() => {
        res.send("Error while updating the item.");
    });
})

export {
    itemRoutes
}